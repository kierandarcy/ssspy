# -*- coding: utf-8 -*-
from setuptools import setup
from ssspy import __version__

setup(
    name='SSSPy',
    version=__version__,
    description='Triple-S for Python',
    author='Kieran Darcy',
    author_email='kdarcy@clientconfident.co.uk',
    url='https://bitbucket.org/clientconfident/ssspy',
    packages=['ssspy'],
    install_requires=[
        'XlsxWriter>=1.0.2',
        'lxml>=4.1.1',
        'six>=1.11.0'
    ],
    extras_require={
        ':python_version=="2.7"': [
            'unicodecsv>=0.9.4'
        ],
    },
)
