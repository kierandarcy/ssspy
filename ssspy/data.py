import datetime
import io
import json
from collections import OrderedDict
from decimal import Decimal

import six
from math import ceil
from six.moves import range

if six.PY2:
    import unicodecsv as csv
else:
    import csv


def roundup(x):
    value = int(ceil(x / 10) * 10)
    return value or 10


def boolean(data):
    data = '{}'.format(data).lower()
    if data in ('yes','true','1','t','y'):
        return True
    elif data in ('no','false','0','f','n'):
        return False
    return None


def from_bitstring(bitstring):
    return [
        code + 1
        for (code, value)
        in enumerate([
            boolean(bit)
            for bit
            in bitstring
        ])
        if value
    ]


def to_bitstring(numbers, size=None):
    if size is None:
        size = max(numbers)
    bitstring = ['0'] * size
    for number in numbers:
        try:
            bitstring[number - 1] = '1'
        except IndexError:
            pass
    return ''.join(bitstring)


def from_spread(number_string, width=1, subfields=None):
    if subfields is None:
        subfields = int(len(number_string) / width)

    r = range(0, subfields * width, width)
    return [
        int(number_string[s:s + width])
        for s
        in r
        if number_string[s:s + width].strip()
    ]


def to_spread(numbers, width=None):
    numbers = [
        int(number)
        for number
        in numbers
    ]

    if width is None:
        width = len('{}'.format(max(numbers)))

    return ''.join([
        '{:0{width}}'.format(number, width=width)
        for number
        in numbers
    ])


class SSSJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            s_obj = u'{}'.format(obj)
            if '.' in s_obj:
                return float(obj)
            return int(obj)
        elif isinstance(obj, datetime.date):
            return obj.strftime('%Y%m%d')
        elif isinstance(obj, datetime.time):
            return obj.strftime('%H%M%S')
        return json.JSONEncoder.default(self, obj)

    
class ValidationError(ValueError): 
    pass


class MultipleTuple(tuple):
    """Adds methods to a list to check that any, all or none of the items
    specifed exist in the iterable"""

    def __new__(self, *args, **kwargs):
        self.variable = kwargs.pop('variable', None)
        return super(self, MultipleTuple).__new__(
            MultipleTuple,
            *args,
            **kwargs
        )

    def any(self, items):
        return any([item in self for item in items])

    def all(self, items):
        return all([item in self for item in items])

    def to_bitstring(self, size=None):
        if size is None and self.variable:
            size = self.variable.values.max_code
        return to_bitstring(numbers=self, size=size)

    def to_bitlist(self, size=None, as_boolean=False):
        if as_boolean is True:
            mapper = boolean
        else:
            mapper = int
        return list(map(mapper, self.to_bitstring(size=size)))

    def to_valuedict(self):
        if self.variable:
            values = self.values
            return {
                value.code: values.get(value.code)
                for value
                in self.variable.values
            }

    @property
    def values(self):
        if self.variable:
            return {
                value.code: value
                for value
                in self.variable.values
                if value.code in self
            }

    def to_spread(self, width=None):
        if width is None and self.variable:
            if self.variable.spread:
                width = self.variable.spread.width
            else:
                width = self.variable.values.max_code_width
        return to_spread(numbers=self, width=width)


def pretty_join(iterator, separator=', ', joiner=' & '):
    if len(iterator) > 1:
        return '{}{}{}'.format(
            separator.join(iterator[:-1]),
            joiner,
            iterator[-1]
        )
    return joiner.join(iterator)


class DataFileReader(object):
    def __init__(self, type, filepath, encoding='utf-8', **kwargs):
        self.type = type
        self.filepath = filepath
        self.encoding = encoding
        self.kwargs = kwargs
        self.opened = False

    def init_reader(self, sss):
        if not hasattr(self, '_initialised'):
            self.open()

            try:
                self.data.seek(0)
            except AttributeError:
                pass

            for _ in range(sss.survey.record.skip):
                try:
                    next(self.data)
                except StopIteration:
                    pass

            self._initialised = True

    def open(self):
        if not self.opened:
            if self.type == 'csv':
                if six.PY2:
                    self.csvfile = open(self.filepath)
                    self.data = csv.reader(self.csvfile, encoding=self.encoding, **self.kwargs)
                else:
                    self.csvfile = io.open(self.filepath, encoding=self.encoding)
                    self.data = csv.reader(self.csvfile, **self.kwargs)
                self.opened = True
            else:
                self.opened = True
                self.data = io.open(self.filepath, encoding=self.encoding, **self.kwargs)
    
    def close(self):
        if self.type == 'csv':
            self.csvfile.close()
        else:
            self.data.close()
    
    def __enter__(self):
        self.open()

    def __exit__(self, type, value, traceback):
        self.close()

    def __iter__(self):
        return self.data.__iter__()


class DictReader(object):
    """Read from a list of dicts"""
    def __init__(self, data, **kwargs):
        self.data = data
        self.kwargs = kwargs
        self.opened = True

    def init_reader(self, sss):
        pass

    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        pass

    def __iter__(self):
        return self.data.__iter__()


class DataReader(object):
    def __init__(self, reader, sss, with_serial=False):
        self.reader = reader
        self.sss = sss
        self.with_serial = with_serial
        self.reader.init_reader(sss=sss)

    @property
    def with_serial(self):
        return self._with_serial
    
    @with_serial.setter
    def with_serial(self, with_serial):
        if not hasattr(self, '_with_serial'):
            self._with_serial = with_serial
        else:
            raise AttributeError("Can't reset with_serial")
    
    @property
    def variables(self):
        return self.sss.survey.record.variables

    @property
    def format(self):
        return self.sss.survey.record.format

    def parse_rawdata(self, rawdata, _lineno=None):
        data_row = DataRow(reader=self, rawdata=rawdata, lineno=_lineno)

        for name, variable in six.iteritems(self.variables):
            try:
                value = rawdata[name]
            except KeyError:
                value = ''
            except TypeError:
                if self.format == 'fixed':
                    value = rawdata[variable.position.start-1:variable.position.finish]
                elif self.format == 'csv':
                    # If it's CSV, don't fail if the column does not exist
                    try:
                        value = rawdata[variable.position.start-1]
                    except IndexError:
                        value = u''

            data_row[name] = DataField(
                variable=variable,
                rawdata=value,
                row=data_row
            )

        if self.with_serial:
            if data_row.serial:
                return data_row.serial.code, data_row
            return _lineno, data_row        
        return data_row

    @property
    def data(self):
        if not hasattr(self, '_data'):
            # If we've not already parsed the rawdata, do that now
            [row for row in self]
        return self._data

    def validate(self, clear_cache=False):
        if clear_cache or not hasattr(self, '_validate'):
            self._validate = {}
            for row in self.rows():
                messages = row.validate()
                if messages:
                    for variable_name, message in messages.items():
                        error = (int('{}'.format(row.serial)), message)
                        try:
                            self._validate[variable_name].append(
                                error
                            )
                        except KeyError:
                            self._validate[variable_name] = [
                                error
                            ]
        return self._validate

    def read(self):
        """Read the data from the reader (if it has not already been loaded)"""
        assert self.data

    def character_size(self, *fields, _roundup=True):
        if not fields:
            fields = list(self.variables.by_type('character'))
        else:
            fields = list(self.variables.by_type('character').by_name(*fields))

        def get_max(codes):
            max_len = max(
                [len(code) for code in codes if code]
                or [0]
            )
            if _roundup:
                return roundup(max_len)
            return max_len

        return {
            key: get_max(codes=codes)
            for key, codes
            in self.group_by(*fields, codes=True).items()
        }

    def group_by(self, *fields, **kwargs):
        codes = kwargs.get('codes', False)
        scores = kwargs.get('scores', False)
    
        if not fields:
            fields = list(self.variables)
    
        by_field = {}
        
        def append_field(field, value):
            try:
                by_field[field].append(value)
            except KeyError:
                by_field[field] = [value]
        
        if codes:
            [
                [
                    append_field(field, row[field].code)
                    for field
                    in fields
                ]
                for row
                in self.rows()
            ]
        elif scores:
            [
                [
                    append_field(field, row[field].score)
                    for field
                    in fields
                ]
                for row
                in self.rows()
            ]
        else:
            [
                [
                    append_field(field, row[field])
                    for field
                    in fields
                ]
                for row
                in self.rows()
            ]
        
        return by_field
    
    def rows(self):
        if self.with_serial:
            return list(self.data.values())
        else:
            return self.data

    def codes(self, non_nulls_only=False):
        if non_nulls_only:
            return [OrderedDict([(key,value) for key,value in six.iteritems(row.codes) if value]) for row in self.rows()]
        return [row.codes for row in self.rows()]

    def json(self, non_nulls_only=False, **kwargs):
        kwargs.update({'cls': kwargs.pop('cls', SSSJSONEncoder)})
        return json.dumps(self.codes(non_nulls_only=non_nulls_only), **kwargs)

    def append(self, rawdata, lineno=None):
        # Parse each row returning
        row = self.parse_rawdata(rawdata, _lineno=lineno)
        # Populate self.data
        if self.with_serial:
            # As an ordered dict
            self._data[row[0]] = row[1]
        else:
            # As a list
            self._data.append(row)
            # return the parsed row (as if it were a normal iterator)
        return row

    def __iter__(self):
        if not hasattr(self,'_data'):
            # If we're iterating for the first time, then create self.data
            if self.with_serial:
                self._data = OrderedDict()
            else:
                self._data = []

            # Loop through the data file
            with self.reader:
                for lineno, rawdata in enumerate(self.reader):
                    # Parse each row returning
                    row = self.append(rawdata, lineno=lineno)
                    yield row
        else:
            # If we have already parsed the data
            # Yield each entry in the normal way
            if self.with_serial:
                for row in list(self.data.items()):
                    yield row
            else:
                for row in self.data:
                    yield row


class DataRow(OrderedDict):
    def __init__(self, reader, rawdata=None, lineno=None, *args, **kwargs):
        self.reader = reader
        self.rawdata = rawdata
        self.lineno = lineno
        super(DataRow, self).__init__(*args, **kwargs)
        
    def validate(self):
        messages = {}
        for name,field in six.iteritems(self):
            try:
                field.validate()
            except ValidationError as err:
                messages.update({name: u'{}'.format(err)})
        return messages

    @property
    def serial_variable(self):
        return self.reader.sss.survey.record.serial_variable

    @property
    def serial(self):
        if self.serial_variable:
            return self.get(self.serial_variable.name)
        return self.lineno

    @property
    def weight_variable(self):
        return self.reader.sss.survey.record.weight_variable

    @property
    def weight(self):
        if self.reader.sss.survey.record.weight_variable:
            return self.get(self.reader.sss.survey.record.weight_variable.name)

    @property
    def data(self):
        return OrderedDict([(name, field.rawdata)
                            for name, field
                            in list(self.items())])

    @property
    def codes(self):
        return OrderedDict([(name, field.code)
                            for name, field
                            in list(self.items())])

    def json(self, **kwargs):
        kwargs.update({'cls': kwargs.pop('cls', SSSJSONEncoder)})
        return json.dumps(self.codes, **kwargs)

    @property
    def scores(self):
        def code_or_score(field):
            if field.variable.has_scores():
                return field.score
            return field.code
        return OrderedDict([(name,code_or_score(field)) for name,field in list(self.items())])

    def __setitem__(self, key, value, *args, **kwargs):
        super(DataRow, self).__setitem__(key, value, *args, **kwargs)
        try:
            value.row = self
        except AttributeError:
            pass

    def __repr__(self):
        serial_variable = self.serial_variable
        if serial_variable:
            serial = self.serial or self.get(serial_variable.name)
            return "<%s(%s=%d)>" % (self.__class__.__name__, serial_variable.name, serial.code)
        if self.lineno is not None:
            return "<%s(%s)>" % (self.__class__.__name__, self.lineno)
        return "<%s()>" % (self.__class__.__name__)


class DataField(object):
    __slots__ = (
        '_variable',
        '_rawdata',
        'row',
        '_value',
        '_code',
        '_score'
    )

    def __init__(self, variable, rawdata, row=None):
        self.variable = variable
        self.rawdata = rawdata
        self.row = row

    @property
    def variable(self):
        return self._variable

    @variable.setter
    def variable(self, variable):
        if not hasattr(self, '_variable'):
            self._variable = variable
        else:
            raise AttributeError("can't set attribute")

    @property
    def rawdata(self):
        return self._rawdata

    @rawdata.setter
    def rawdata(self, rawdata):
        # Start by clearing any cached properties
        for field in ('_value', '_code', '_score'):
            try:
                delattr(self, field)
            except AttributeError:
                pass
        # Now, we can assign rawdata
        self._rawdata = rawdata

    @property
    def filter(self):
        filter_field = self.filter_field
        if filter_field is not None:
            return filter_field.code
        return True

    @property
    def filter_name(self):
        return self.variable.filter

    @property
    def filter_field(self):
        return self.row.get(self.filter_name)

    @property
    def filter_variable(self):
        field = self.filter_field
        if field:
            return field.variable

    def get_value(self, as_dict=False):
        if self.variable.type == 'multiple':
            if self.code is not None:
                _value = self.code.values
                if not as_dict:
                    _value = list(_value.values())
            else:
                if as_dict:
                    _value = {}
                else:
                    _value = None
        else:
            values = [
                value
                for value
                in self.variable.values
                if value.code == self.code
            ]
            if len(values) == 1:
                _value = values[0]
            else:
                _value = None
        return _value

    @property
    def value(self):
        if not hasattr(self, '_value'):
            self._value = self.get_value()
        return self._value
    
    @property
    def parse_data(self):
        return self.variable.parse_data
    
    @property
    def code(self):
        if not hasattr(self, '_code'):
            self._code = self.parse_data(rawdata=self.rawdata)
        return self._code

    @code.setter
    def code(self, _code):
        try:
            delattr(self, '_value')
        except AttributeError:
            pass
        self._code = _code

    def csv_data(self):
        # Return this data formatted for CSV
        # TODO: This looks like is belongs in Variable
        if self.code is not None:
            if self.variable.type == 'multiple':
                if self.variable.spread:
                    return self.variable.spread.join_data(self.code)
                return self.code.to_bitstring()
            elif self.variable.type == 'date':
                return self.code.strftime('%Y%m%d')
            elif self.variable.type == 'time':
                return self.code.strftime('%H%M%S')
            elif self.variable.type == 'logical':
                return int(self.code)
        return self.code

    def label(self, pretty=False, separator=', ', joiner=' & ',
              template='{}'):
        label_ = ''
        if self.code is not None:
            if self.value is not None:
                if self.variable.type == 'multiple':
                    label_ = [
                        template.format(value.text)
                        for value
                        in self.value
                    ]
                else:
                    label_ = template.format(self.value.text)
            else:
                if self.variable.type == 'date':
                    label_ = template.format(self.code.strftime('%Y-%m-%d'))
                elif self.variable.type == 'time':
                    label_ = template.format(self.code.strftime('%H:%M:%S'))
                else:
                    label_ = template.format(self.code)

        if pretty and self.variable.type == 'multiple':
            return pretty_join(
                iterator=label_,
                separator=separator,
                joiner=joiner
            )
        return label_

    @property
    def score(self):
        if not hasattr(self, '_score'):
            self._score = None
            if self.value is not None:
                if self.variable.type == 'single':
                    self._score = self.value.score
        return self._score

    def recode(self, recodes):
        # Given a dict of {
        # 'old_value1': 'new_value1',
        # 'old_value2': 'new_value2'
        # }
        # recode this data
        if self.any(list(recodes)):
            if self.variable.type == 'multiple':
                if self.code is not None:
                    self.code = MultipleTuple([
                        recodes.get(c, c)
                        for c
                        in self.code
                    ], variable=self.variable)
            else:
                self.code = recodes.get(
                    self.code,
                    self.code
                )

    def validate(self, _code=None):
        messages = []
        _code = _code or self.code
        if not _code and self.variable.type == 'multiple':
            _code = None

        if _code is not None:
            if not self.filter:
                messages.append(
                    u'Non-empty value {value!r}'
                    u' when the filter {filter_name!r} is False.'
                    .format(
                        value=_code,
                        filter_name=self.filter_name
                    )
                )

            if self.variable.type == 'logical':
                if _code not in (True, False):
                    messages.append(
                        u'{} is not a valid code for {}'
                            .format(_code, self.variable.name)
                    )
            elif self.variable.type == 'character':
                if _code and len(_code) > self.variable.size:
                    messages.append(
                        u'Answer too long: {!r}, allowable size={}, '
                        u'actual size={}'
                            .format(
                                self.variable.name,
                                self.variable.size,
                                len(_code)
                            )
                    )
            else:
                codes = _code
                if not self.variable.type == 'multiple':
                    codes = [_code]

                for code in codes:
                    if not self.variable.values.valid_code(code):
                        messages.append(
                            u'{!r} is not a valid code for {!r}'
                                .format(code, self.variable.name)
                        )
        if messages:
            raise ValidationError(u'; '.join(messages))

    def copy_to(self, variable_name):
        """Copy this field's raw data to the named variable for this row"""
        if variable_name != self.variable.name:
            try:
                new_field = self.row[variable_name]
                new_field.rawdata = self.rawdata
            except KeyError:
                raise AttributeError(
                    u'cannot copy, {!r} does not exist in this row'
                        .format(variable_name)
                )
            except TypeError:
                raise AttributeError(
                    u'cannot copy to {!r}, this field is not attached to a row'
                        .format(variable_name)
                )

    def __str__(self):
        return self.label(pretty=True)

    def __repr__(self):
        return '<DataField({name!r}, {type!r})>'.format(
            name=self.variable.name,
            type=self.variable.type
        )
    
    def __nonzero__(self):
        if self.variable.type == 'quantity':
            return bool(u'{}'.format(self.rawdata).strip())        
        return any([self.value, self.code])
    
    def __contains__(self, item):
        try:
            return self.code.__contains__(item)
        except AttributeError:
            if self.code is None:
                return item is None
            return False
        except TypeError:
            return False
        return False
    
    def __lt__(self, item):
        try:
            return self.code.__lt__(item)
        except AttributeError:
            pass
        return self.code < item
    
    def __le__(self, item):
        try:
            return self.code.__le__(item)
        except AttributeError:
            pass
        return self.code <= item
    
    def __eq__(self, item):
        try:
            return self.code.__eq__(item)
        except AttributeError:
            pass
        return self.code == item
    
    def __ne__(self, item):
        try:
            return self.code.__ne__(item)
        except AttributeError:
            pass
        return self.code != item
    
    def __gt__(self, item):
        try:
            return self.code.__gt__(item)
        except AttributeError:
            pass
        return self.code > item
    
    def __ge__(self, item):
        try:
            return self.code.__ge__(item)
        except AttributeError:
            pass
        return self.code >= item
    
    lt = __lt__
    le = __le__
    lt = __lt__
    eq = __eq__
    ne = __ne__
    gt = __gt__
    ge = __ge__

    def any(self, items):
        if hasattr(self.code, 'any'):
            return self.code.any(items)
        return self.code in items

    def all(self, items):
        if hasattr(self.code, 'all'):
            return self.code.all(items)
        return self.code == items
