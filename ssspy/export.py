import decimal
import io
import json as json_
import os.path

import six
import xlsxwriter
import pandas as pd

if six.PY2:
    import unicodecsv as _csv
else:
    import csv as _csv

from .meta import load
from .data import DataReader


def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        try:
            return int(str(obj))
        except ValueError:
            return float(str(obj))
    raise TypeError


def json(sss_filename_or_reader,
         datafile_filename=None,
         json_filename=None,
         datafile_encoding='utf-8',
         repair=None,
         exclude_variables=None,
         use_scores=False, indent=4):
    if exclude_variables is None:
        exclude_variables = []

    if isinstance(sss_filename_or_reader, DataReader):
        data = sss_filename_or_reader
        if json_filename is None:
            json_filename = '{}.json'.format(
                os.path.splitext(sss_filename_or_reader.reader.filepath)[0])
    else:
        if json_filename is None:
            json_filename = '{}.json'.format(sss_filename_or_reader)
        data = load(sss_filename_or_reader,
                    datafile_path=datafile_filename,
                    datafile_encoding=datafile_encoding,
                    repair=repair)
    
    json_data = []
    if data.with_serial:
        for row, (id, fields) in enumerate(data):
            if use_scores:
                json_data.append(fields.scores)
            else:
                json_data.append(fields.codes)
    else:
        for row, fields in enumerate(data):
            if use_scores:
                json_data.append(fields.scores)
            else:
                json_data.append(fields.codes)

    with io.open(json_filename, 'w', encoding=datafile_encoding) as outfile:
        outfile.write(json_.dumps(json_data,
                                  default=decimal_default,
                                  indent=indent))


def csv(sss_filename_or_reader,
        datafile_filename=None,
        csv_filename=None,
        datafile_encoding='utf-8',
        repair=None,
        exclude_variables=None,
        write_fieldnames=False):
    if exclude_variables is None:
        exclude_variables = []

    if isinstance(sss_filename_or_reader, DataReader):
        data = sss_filename_or_reader
        if csv_filename is None:
            csv_filename = '{}.csv'.format(os.path.splitext(
                sss_filename_or_reader.reader.filepath)[0])
    else:
        if csv_filename is None:
            csv_filename = '{}.csv'.format(sss_filename_or_reader)
        data = load(sss_filename_or_reader,
                    datafile_path=datafile_filename,
                    datafile_encoding=datafile_encoding,
                    repair=repair)
    
    variables = [variable.name
                 for variable
                 in list(data.sss.survey.record.variables.values())
                 if variable.name not in exclude_variables]

    with io.open(csv_filename, 'w', encoding=datafile_encoding) as outfile:
        csv_writer = _csv.writer(outfile)
        if write_fieldnames:
            csv_writer.writerow(variables)

        if data.with_serial:
            for row, (id, fields) in enumerate(data.items()):
                csv_writer.writerow([data_field.csv_data()
                                     for data_field
                                     in fields.values()])
        else:
            for row, fields in enumerate(data):
                try:
                    csv_data = [data_field.csv_data()
                                for data_field
                                in fields.values()]
                    csv_writer.writerow(csv_data)
                except AttributeError as err:
                    import pdb; pdb.set_trace()
                    raise err


def excel(sss_filename_or_reader,
          datafile_filename=None,
          xlsx_filename=None,
          datafile_encoding='utf-8',
          repair=None,
          exclude_variables=None,
          auto_close=True,
          header_template='{variable.name}: {variable}'):
    if exclude_variables is None:
        exclude_variables = []

    if isinstance(sss_filename_or_reader, DataReader):
        data = sss_filename_or_reader
        if xlsx_filename is None:
            xlsx_filename = '{}.xlsx'.format(
                os.path.splitext(sss_filename_or_reader.reader.filepath)[0])
    else:
        if xlsx_filename is None:
            xlsx_filename = '{}.xlsx'.format(sss_filename_or_reader)
        data = load(sss_filename_or_reader,
                    datafile_path=datafile_filename,
                    datafile_encoding=datafile_encoding,
                    repair=repair)
    
    workbook = xlsxwriter.Workbook(xlsx_filename)
    worksheet = workbook.add_worksheet('Data')
    bold = workbook.add_format({'bold': True})

    col = 0

    variables = [variable
                 for variable
                 in list(data.sss.survey.record.variables.values())
                 if variable.name not in exclude_variables]
    for col, variable in enumerate(variables):
        worksheet.write(0, col, header_template.format(variable=variable), bold)

    def process_fields(row, fields):
        variables = [(name,field)
                     for name,field
                     in list(fields.items())
                     if name not in exclude_variables]
        for col, (name,field) in enumerate(variables):
            worksheet.write(row+1,
                            col,
                            field.label(pretty=True,
                                        separator='; ',
                                        joiner='; '))
    
    if data.with_serial:
        for row, (id,fields) in enumerate(data):
            process_fields(row, fields)
    else:
        for row, fields in enumerate(data):
            process_fields(row, fields)

    worksheet.freeze_panes(1, 0)
    worksheet.autofilter(0, 0, 0, col)
    
    if auto_close:
        workbook.close()

    return workbook


def dataframe(
        sss_filename_or_reader,
        datafile_filename=None,
        datafile_encoding='utf-8',
        repair=None):

    if isinstance(sss_filename_or_reader, DataReader):
        data = sss_filename_or_reader
    else:
        data = load(sss_filename_or_reader,
                    datafile_path=datafile_filename,
                    datafile_encoding=datafile_encoding,
                    repair=repair)

    multiple_variables = {
        variable.name: [
            '{}#{}'.format(variable.name, code)
            for code
            in variable.values.codes
        ]
        for variable
        in data.sss.variables.by_type('multiple').values()
    }

    def columns():
        for variable in data.sss.variables.values():
            if variable.name in multiple_variables:
                for name in multiple_variables[variable.name]:
                    yield name
            else:
                yield variable.name

            if variable.has_scores():
                yield '{}.score'.format(variable.name)

    grouped = data.group_by()

    def score_picker(variable):
        if variable.has_scores():
            def picker(field):
                if field.score is not None:
                    return float(field.score)
        else:
            def picker(field):
                return field.code
        return picker

    dataframe = {}

    for name, rows in grouped.items():
        variable = data.sss.variables[name]
        pick = score_picker(variable=variable)

        index, strings, scores = [], [], []
        if variable.type == 'multiple':
            rows_data = {
                d.row.serial.code: {
                    '{}#{}'.format(variable.name, code): '{}'.format(value)
                    for code, value
                    in d.code.values.items()
                }
                for d
                in rows
                if d.code
            }
        else:
            rows_data = [
                (d.row.serial.code, '{}'.format(d), pick(field=d))
                for d
                in rows
                if d.code is not None
            ]

            if rows_data:
                index, strings, scores = zip(*rows_data)

        if variable.type == 'single':
            dataframe[name] = pd.Series(
                pd.Categorical(
                    strings,
                    categories=variable.values.texts
                ),
                index=index,
                name=name,
                dtype='category'
            )

            if variable.has_scores():
                dataframe['{}.score'.format(name)] = pd.Series(
                    scores,
                    index=index,
                    name='{}.score'.format(name),
                )
        elif variable.type == 'multiple':
            # Reshape for each sub field
            field_data = {}
            for idx, idx_data in rows_data.items():
                for field_name, field_value in idx_data.items():
                    try:
                        field_data[field_name].append((idx, field_value))
                    except KeyError:
                        field_data[field_name] = [(idx, field_value)]

            for field_name in multiple_variables[name]:
                field_data_rows = field_data.get(field_name)
                if field_data_rows:
                    index, strings = zip(*field_data_rows)
                else:
                    index, strings = [], []

                dataframe[field_name] = pd.Series(
                    pd.Categorical(
                        strings,
                        categories=variable.values.texts
                    ),
                    index=index,
                    name=field_name,
                    dtype='category'
                )
        elif variable.type == 'character':
            dataframe[name] = pd.Series(
                strings,
                index=index,
                name=name,
            )
        else:
            dataframe[name] = pd.Series(
                scores,
                index=index,
                name=name,
            )

    df = pd.DataFrame(dataframe, columns=list(columns()))

    return df
