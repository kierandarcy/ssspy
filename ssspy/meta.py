"""Defines the Triple-S Meta Data file"""

import decimal
import string
import os
import getpass
import re
import datetime
import time
import copy
from collections import OrderedDict

import six
from six.moves import range
from lxml import etree

from .data import (
    DataReader,
    DataFileReader,
    MultipleTuple,
    boolean,
    to_spread,
    to_bitstring,
    from_bitstring,
    from_spread
)


SSS_NAME_RE = re.compile('[A-Za-z_][A-Za-z0-9\._]+')


class SSSError(Exception): pass


def validate_name(name, variables=None, pattern=SSS_NAME_RE):
    """
    Validate a `name` for this variable. In order to
    be generally useful these "names" have a restricted definition:

    1. names must start with a letter (A-Z or a-z) or _ (underscore)
       character. subsequent characters can be letters (A-Z or a-z),
       digits (0-9), _ (underscore), or . (period) characters.
    2. names are case sensitive (i.e. upper and lower case are
       different)
    3. names must be unique within their type
    4. names must not begin, or end, with whitespace (e.g. spaces,
       tabs), neither should they contain embedded whitespace
    5. names may be unlimited in length

    Given a potential name, validate the name against the given
    re pattern and return the name if it conforms to the pattern
    and the name is not already associated with an existing variable
    in this variables parent `record`.

    If the name is not valid, raise an AttributeError.
    """
    if variables is None:
        variables = {}
    match = re.match(pattern, name)
    if match and match.group() == name:
        if name in variables:
            raise AttributeError('Variable names must be unique '
                                 'within this survey type: {}'.format(name))
    else:
        raise AttributeError('Variable names must conform to '
                             'the Triple-S standard: {}'.format(name))
    return name


class Variables(OrderedDict):
    @property
    def serial(self):
        for v in self.values():
            if v.use == 'serial':
                return v

    @property
    def serial_name(self):
        serial = self.serial
        if serial:
            return serial.name

    @property
    def weight(self):
        for v in self.values():
            if v.use == 'weight':
                return v

    @property
    def weight_name(self):
        weight = self.weight
        if weight:
            return weight.name

    def have_scores(self, _drop=False):
        return Variables(
            (k, v)
            for k, v
            in self.items()
            if v.has_scores() is not _drop
        )

    def not_have_scores(self):
        return self.have_scores(_drop=True)

    def by_name(self, *names, 
                startswith=None, endswith=None, regex=None, _drop=False):
        if regex:
            def _check(variable):
                return bool(re.search(regex, variable.name))
        elif startswith:
            def _check(variable):
                return variable.name in names or \
                       variable.name.startswith(startswith)
        elif endswith:
            def _check(variable):
                return variable.name in names or \
                       variable.name.endswith(endswith)
        else:
            def _check(variable):
                return variable.name in names

        return Variables(
            (k, v)
            for k, v
            in self.items()
            if _check(v) is not _drop
        )

    def drop_by_name(self, *names, startswith=None):
        return self.by_name(*names, startswith=startswith, _drop=True)

    def literals(self, drop=False):
        return Variables(
            (k, v)
            for k, v
            in self.items()
            if (v.type == 'single' and v.format == 'literal') is not drop
        )

    def floats(self, drop=False):
        return Variables(
            (k, v)
            for k, v
            in self.items()
            if (v.type == 'quantity' and v.values.range.is_float()) is not drop
        )

    def by_type(self, *types, _drop=False):
        return Variables(
            (k, v)
            for k, v
            in self.items()
            if (v.type in types) is not _drop
        )

    def drop_by_type(self, *types):
        return self.by_type(*types, _drop=True)

    def by_use(self, *uses, _drop=False):
        return Variables(
            (k, v)
            for k, v
            in self.items()
            if (v.use in uses) is not _drop
        )

    def drop_by_use(self, *uses):
        return self.by_use(*uses, _drop=True)

    def drop_serial(self):
        return self.by_use('serial', _drop=True)

    def by_label(self, *labels, exact=True, _drop=False):
        if exact:
            def matcher(items, v):
                return any(
                    item in v.label
                    for item
                    in items
                )
        else:
            def matcher(items, v):
                return any(
                    item.lower() in v.label.lower()
                    for item
                    in items
                )

        return Variables(
            (k,v)
            for k,v
            in self.items()
            if matcher(items=labels, v=v) is not _drop
        )

    def drop_by_label(self, *labels, exact=True):
        return self.by_label(*labels, exact=exact, _drop=True)

    def by_value_code(self, *codes, _drop=False):
        def matcher(items, v):
            return any(
                item in v.values.codes
                for item
                in items
            )

        return Variables(
            (k,v)
            for k,v
            in self.items()
            if matcher(items=codes, v=v) is not _drop
        )

    def drop_by_value_code(self, *codes):
        return self.by_value_code(*codes, _drop=True)

    def by_value(self, *values, exact=True, _drop=False):
        if exact:
            def matcher(items, v):
                return any(
                    item in v.values.texts
                    for item
                    in items
                )
        else:
            def matcher(items, v):
                return any(
                    item.lower() in ' '.join(v.values.texts).lower()
                    for item
                    in items
                )

        return Variables(
            (k,v)
            for k,v
            in self.items()
            if matcher(items=values, v=v) is not _drop
        )

    def drop_by_value(self, *values, exact=True):
        return self.by_value(*values, exact=exact, _drop=True)

    def by_filter(self, *filters, _drop=False):
        return Variables(
            (k,v)
            for k,v
            in self.items()
            if (v.filter in filters) is not _drop
        )

    def drop_by_filter(self, *filters):
        return self.by_filter(*filters, _drop=True)

    def to_python(self):
        return OrderedDict([
            (k, v.to_python())
            for k, v
            in self.items()
        ])


class MetaReader(object):
    """Read a Triple-S XML file."""

    __parser__ = etree.XMLParser(dtd_validation=True)

    def __init__(self, filepath, validate=False):
        self.filepath = filepath
        self.validate = validate

    @property
    def validate(self):
        return self._validate

    @validate.setter
    def validate(self, validate):
        if not hasattr(self, '_validate'):
            self._validate = validate
            self.parse()
        else:
            raise AttributeError("can't set attribute")

    @property
    def xml(self):
        return self._xml

    @property
    def parser(self):
        if self.validate:
            return self.__parser__

    def parse(self):
        self.variables = []
        self._xml = etree.parse(self.filepath, parser=self.parser)

    @property
    def sss(self):
        if not hasattr(self, '_sss'):
            self._sss = self.xml.getroot()
        return self._sss

    @property
    def survey(self):
        if not hasattr(self, '_survey'):
            self._survey = self.sss.find('survey')
        return self._survey

    @property
    def heirarchy(self):
        if not hasattr(self, '_heirarchy'):
            self._survey = self.sss.find('heirarchy')
        return self._heirarchy

    @property
    def record(self):
        if not hasattr(self, '_record'):
            if self.survey is not None:
                self._record =  self.survey.find('record')
        return self._record


def parse(filepath, validate=False, serial_variable=None, with_warnings=True):
    """
    Given the path to a Triple-S XML meta file, parse the XML and use it
    to construct an instance of a SSS object.
    """
    kwargs = {}
    xml = MetaReader(filepath, validate=validate)

    record = Record()
    record.ident = xml.record.attrib.get('ident')
    record.format = xml.record.attrib.get('format','fixed')
    record.skip = int(xml.record.attrib.get('skip',0))
    record.href = xml.record.attrib.get('href')

    def get_value_text(value_element, default='Code {!r} **text missing**'):
        if value_element.text is None:
            return default.format(value_element.attrib.get('code'))
        return value_element.text.strip()

    for el in xml.record:
        if el.tag == 'variable':
            position = el.find('position').attrib
            name = el.find('name').text.strip()
            label = el.find('label').text.strip()
            position = el.find('position').attrib
            size = el.find('size')
            spread = el.find('spread')
            filter = el.find('filter')
            if filter is not None:
                filter = filter.text.strip() or None
            kwargs = el.attrib
            # If this is flagged as the serial variable, flag it
            if name == serial_variable:
                kwargs['use'] = 'serial'
            elif serial_variable:
                try:
                    del kwargs['use']
                except KeyError:
                    pass
            variable = Variable(record, name, label, **kwargs)
            variable.position.start = position.get('start')
            variable.position.finish = position.get('finish')

            variable.filter = filter

            if size is not None:
                variable.size = int(size.text.strip())

            if spread is not None:
                variable.spread = spread.attrib

            values = el.find('values')
            if values is not None:
                range_ = values.find('range')
                if range_ is not None:
                    variable.values.range = range_.attrib

                for value in values.findall('value'):
                    if value.text is None:
                        if with_warnings:
                            print("Warning: The variable {!r} has a value with "
                                  "an empty label".format(name))

                    variable.values.append(Value(variable.values,
                                                 get_value_text(value),
                                                 value.attrib.get('code'),
                                                 value.attrib.get('score')))

            #record.variables.append(variable)
            #record.variables[variable.name] = variable

    survey_options = dict([(el.tag,el.text.strip())
                           for el
                           in xml.survey
                           if isinstance(el.tag, six.string_types)])
    survey_options.pop('survey', None)
    survey_options.pop('record', None)
    
    sss_options = dict([(el.tag,el.text.strip())
                        for el
                        in xml.sss
                        if isinstance(el.tag, six.string_types)])
    sss_options.pop('survey', None)
    sss_options.pop('record', None)
    
    survey = Survey(record=record, **survey_options)
    sss = SSS(version=xml.sss.attrib.get('version'),
              survey=survey, **sss_options)
    #sss.record = record
    return sss


def load(metafile_path,
         datafile_path=None,
         datafile_encoding='utf-8',
         repair=None,
         with_serial=True,
         serial_variable=None,
         with_warnings=True,
         **kwargs):
    """
    Read, parse and load Triple-S meta data and its associated data file. Returns
    and instance of a DataReader object.
    """
    
    # If the metafile path does not have the SSS extenstion, add it.
    if os.path.splitext(metafile_path)[1].lower() not in ('.sss',):
        metafile_path = '%s.sss' % metafile_path

    # If we've been asked to repair the meta file first pass it to the provided callable 
    # The callable should make inline changes to the specified file if necessary.
    if callable(repair):
        repair(metafile_path)
    
    # Parse the SSS meta file
    meta = parse(metafile_path,
                 serial_variable=serial_variable,
                 with_warnings=with_warnings)

    # If we've not been given a datafile_path, then assume it is derived from
    # the metafile_path.
    # TODO Read the meta file to get the optional record[href] attribute.
    if datafile_path is None:
        ext = 'asc'
        if meta.survey.record.format == 'csv':
            ext = 'csv'
        datafile_path = '%s.%s' % (os.path.splitext(metafile_path)[0], ext)

    # Decide upon the best reader for the data file
    # TODO (Kieran) Should this be here, or should it be in DataReader?
    reader = DataFileReader(meta.survey.record.format,
                            datafile_path,
                            encoding=datafile_encoding,
                            **kwargs)
    #if meta.survey.record.format == 'csv':
    #    f = csv.reader(open(datafile_path), encoding=datafile_encoding, **kwargs)
    #else:
    #    f = open(datafile_path, encoding=datafile_encoding, **kwargs)
    #return DataReader(f, meta, with_serial=with_serial)

    return DataReader(reader,
                      meta,
                      with_serial=with_serial)


def load_data(*args, **kwargs):
    return load(*args, **kwargs).data


class XMLObject(object):
    etree = etree
    
    def __xml__(self):
        raise NotImplementedError


class SSSDescription(XMLObject):
    __versions__ = ('1.1', '1.2', '2.0')
    __doctype__ = u'<!DOCTYPE sss PUBLIC "-//triple-s//DTD Survey Interchange v2.0//EN" "http://www.triple-s.org/dtd/sss_v20.dtd">'
    __txtmodes__ = ('interview', 'analysis')
    __attributes__ = ('languages', 'modes')
    # Node names with default values (as callables)
    __nodes__ = OrderedDict([
        ('date', lambda: datetime.datetime.utcnow().date()),
        ('time', lambda: u'{} UTC'.format(datetime.datetime.utcnow().time().strftime('%H:%M:%S'))),
        ('origin', lambda: u'SSSPy'),
        ('user', lambda: getpass.getuser()),
    ])

    def __init__(self, version='2.0', **options):
        self.version = version
        self.options = options

        #attributes
        self._init_attributes()

        #nodes
        self._init_nodes()

    def _init_attributes(self):
        for attr in self.__attributes__:
            setattr(self, attr, self.options.get(attr))

    def _init_nodes(self):
        for node in self.__nodes__:
            setattr(self, node, self.options.get(node))

    def reset_nodes(self):
        for node, default_value in list(self.__nodes__.items()):
            setattr(self, node, default_value())

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        if not hasattr(self, '_version'):
            if version in self.__versions__:
                self._version = version
            else:
                raise ValueError('"%s" is not a valid SSS version' % version)
        else:
            raise AttributeError("can't set attribute")

    @property
    def languages(self):
        return self._languages

    @languages.setter
    def languages(self, languages):
        if languages is None:
            self._languages = []
        elif isinstance(languages, six.string_types):
            self._languages = languages.split()
        else:
            self._languages = list(languages)

    @property
    def modes(self):
        return self._modes

    @modes.setter
    def modes(self, modes):
        if modes is None:
            _modes = list(self.__txtmodes__)
        elif isinstance(modes, six.string_types):
            _modes = modes.lower().split()
        else:
            _modes = list(modes)

        if set(_modes) <= set(self.__txtmodes__):
            self._modes = _modes
        else:
            raise ValueError('"%s" is not a valid SSS modes list' % modes)

    def xml(self, with_doctype=False):
        if with_doctype:
            xml = self.etree.XML(
                    self.etree.tounicode(
                        self.etree.ElementTree(self.__xml__()),
                        doctype=self.__doctype__)
            )
        else:
            xml = self.__xml__()
        return self.etree.ElementTree(xml)

    def write(self, f, with_doctype=True, **kwargs):
        # Write our own nodes
        self.reset_nodes()
        
        if 'encoding' not in kwargs:
            kwargs['encoding'] = 'UTF-8'
        if 'xml_declaration' not in kwargs:
            kwargs['xml_declaration'] = True
        if 'pretty_print' not in kwargs:
            kwargs['pretty_print'] = True

        xml = self.xml(with_doctype=with_doctype)
        kwargs.pop('with_doctype', None)
        xml.write(f, **kwargs)

    def __xml__(self):
        attrib = OrderedDict([('version', u'{}'.format(self.version))])
        if self.languages:
            attrib['languages'] = u' '.join(self.languages)
        if self.modes:
            attrib['modes'] = u' '.join(self.modes)
        elem = self.etree.Element('sss', **attrib)
        for n in self.__nodes__:
            _node = getattr(self, n)
            if _node:
                node = self.etree.Element(n)
                node.text = u'{}'.format(_node)
                elem.append(node)
        return elem

    def to_python(self):
        obj = {
            'version': self.version,
            'languages': self.languages,
            'modes': self.modes,
        }

        for n in self.__nodes__:
            _node = getattr(self, n)
            obj[n] = _node

        return obj


class Hierarchy(SSSDescription):
    def hierarchy(self):
        self.kwargs.get('hierarchy')


class SSS(SSSDescription):
    def __init__(self, *args, **kwargs):
        survey = kwargs.pop('survey', None)
        super(SSS, self).__init__(*args, **kwargs)
        if survey:
            self.survey = survey

    @property
    def survey(self):
        return self._survey

    @survey.setter
    def survey(self, survey):
        if isinstance(survey, Survey):
            self._survey = survey
        else:
            raise AttributeError('"%s" is not an instance of <Survey>')

    @property
    def variables(self):
        return self.survey.record.variables

    def __xml__(self):
        elem = super(SSS, self).__xml__()
        elem.append(self.survey.__xml__())
        return elem

    def to_python(self):
        obj = super(SSS, self).to_python()
        obj['survey'] = self.survey.to_python()
        return obj


class Survey(XMLObject):
    __attributes__ = ()
    __nodes__ = ('name','version','title')

    def __init__(self, record=None, **options):
        if record:
            self.record = record
        self.options = options
        #attributes
        self._init_attributes()
        #nodes
        self._init_nodes()

    @property
    def record(self):
        return self._record

    @record.setter
    def record(self, record):
        if isinstance(record, Record):
            self._record = record
        else:
            raise AttributeError('"%s" is not an instance of <Record>')

    def _init_attributes(self):
        for attr in self.__attributes__:
            setattr(self, attr, self.options.get(attr))

    def _init_nodes(self):
        for node in self.__nodes__:
            setattr(self, node, self.options.get(node))

    def __xml__(self):
        elem = self.etree.Element('survey')
        for n in self.__nodes__:
            _node = getattr(self, n)
            if _node:
                node = self.etree.Element(n)
                node.text = u'{}'.format(_node)
                elem.append(node)
        elem.append(self.record.__xml__())
        return elem

    def to_python(self):
        obj = {'record': self.record.to_python()}
        for n in self.__nodes__:
            _node = getattr(self, n)
            if _node:
                obj[n] = _node
        return obj

    def __repr__(self):
        return '<Survey()>'.format(self=self)


class Record(XMLObject):
    __idents__ = '%s' % string.ascii_letters
    __recfmts__  = ('fixed','csv')

    def __init__(self, ident='A', format='fixed', skip=0, href=None):
        self.ident = ident
        self.format = format
        self.skip = skip
        self.href = href
        self.variables = Variables()
        # unique variables
        self.serial_variable = None
        self.weight_variable = None

    @property
    def ident(self):
        return self._ident

    @ident.setter
    def ident(self, ident):
        ident = '%s' % ident
        if ident in self.__idents__:
            self._ident = ident
        else:
            raise ValueError('"%s" is not a valid SSS record ident' % ident)

    @property
    def format(self):
        return self._format

    @format.setter
    def format(self, format):
        if format in self.__recfmts__:
            self._format = format
        else:
            raise ValueError('"%s" is not a valid SSS record format' % format)

    @property
    def variables(self):
        return self._variables

    @variables.setter
    def variables(self, variables):
        self._variables = Variables()
        for name,variable in list(variables.items()):
            variable.record = self

    @property
    def fieldnames(self):
        return list(self.variables.keys())

    @property
    def variable_idents(self):
        return [v.ident for v in list(self.variables.values())]

    @property
    def next_variable_ident(self):
        idents = self.variable_idents
        if idents:
            return max(idents) + 1
        return 1

    @property
    def colspecs(self):
        return OrderedDict([
            (var.name, var.position.colspec())
            for var
            in list(self.variables.values())
        ])

    def rename_variables(self, variables):
        for old_name, new_name in variables.items():
            variable = self.variables.get(old_name)
            if variable:
                variable.rename(new_name)

    def drop_variable(self, variable_name):
        variable = self.variables.get(variable_name)
        if variable:
            del self.variables[variable_name]
            variable.record = None
        return variable

    def change_format_to_csv(self, skip=None, colspec=None):
        if skip is not None:
            self.skip = skip
        
        if colspec is None:
            colspec = {}

        if self.format == 'fixed':
            for order, variable in enumerate(self.variables.values(), 1):
                variable.position.start = colspec.get(variable.name, order)
                variable.position.finish = None

            self.format = 'csv'

            return True
        return False
    
    def sort(self):
        """Sort variables by ident"""
        self._variables = Variables(sorted(list(self.variables.items()),
                                             key=lambda item: item[1].ident))
    
    def __len__(self):
        return len(self.variables)

    def __repr__(self):
        return '<Record(ident={self.ident!r}, format={self.format!r})>'.format(self=self)
    
    def __xml__(self):
        attrib = OrderedDict([('ident', u'{}'.format(self.ident))])
        if self.href:
            attrib['href'] = u'{}'.format(self.href)
        if not self.format == 'fixed':
            attrib['format'] = u'{}'.format(self.format)
        if self.skip:
            attrib['skip'] = u'{}'.format(self.skip)
        elem = self.etree.Element('record', **attrib)
        for variable in list(self.variables.values()):
            elem.append(variable.__xml__())
        return elem

    def to_python(self):
        obj = {
            'ident': self.ident,
            'variables': self.variables.to_python(),
        }

        if self.href:
            obj['href'] = self.href

        if not self.format == 'fixed':
            obj['format'] = self.format

        if self.skip:
            obj['skip'] = self.skip

        return obj


class Variable(XMLObject):
    __vartypes__ = (
        'single',
        'multiple',
        'quantity',
        'character',
        'logical',
        'date',
        'time',
    )
    __values_types__ = (
        'single',
        'multiple',
        'quantity',
        'date',
        'time',
    )
    __usetypes__ = {
        'serial': (
            'character',
            'quantity',
        ),
        'weight': (
            'quantity',
        ),
    }
    __varfmts__ =  (
        'literal',
        'numeric',
    )

    def validate_name(self, name):
        return validate_name(name=name, variables=self.record.variables)

    def __init__(self, record, name, label, type, ident=None, use=None,
                 format='numeric', filter=None):
        if ident is None:
            ident = record.next_variable_ident

        self.record = record
        self.name = name
        self.label = label
        self.type = type
        self.ident = ident
        self.use = use
        self.format = format
        self.filter = filter
        self.position = Position(self)
        self.values = Values(self)
        self.spread = None
        self.size = None
        
        # Associate this with the record
        self.record.variables[self.name] = self

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, values):
        # If we've been given an existing values object, 
        # we'll convert it to a dict (to make a copy).
        if isinstance(values, Values):
            values = values.to_python()

        # If we have a dict, it should contain its data in 
        # the for {'values': [ list of values], 'range': { dict of range }}
        if isinstance(values, dict):
            _range = values.get('range')
            values = values.get('values', [])
        # `values` should be in iterable, so we'll not alter it
        else:
            _range = None

        # Create a new empty Values object
        self._values = Values(variable=self)

        # Populate any values
        for value in values:
            self._values.append(
                Value(
                    values=self._values,
                    **value
                )
            )

        # Apply the range
        self._values.range = _range

    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, name):
        if not hasattr(self, '_name'):
            self._name = self.validate_name(name)
        else:
            raise AttributeError('cannot rename a variable directly')

    @property
    def spread(self):
        return self._spread

    @spread.setter
    def spread(self, spread):
        self._spread = None
        if self.type == 'multiple':
            if spread is not None:
                self._spread = Spread(self, **spread)

    @property
    def size(self):
        if self.type == 'character':
            if self._size is None:
                return self.position.length()
            return self._size

    @size.setter
    def size(self, size):
        try:
            size = int(size)
            if self.position.length():
                if size > 0  and size <= self.position.length():
                    self._size = size
                else:
                    raise AttributeError('size must be between 1 and %d' % self.size)
            else:
                self._size = size
        except TypeError:
            self._size = None

    @size.deleter
    def size(self):
        self.size = None

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, type_):
        if not hasattr(self, '_type'):
            if type_ in self.__vartypes__:
                self._type = type_
            else:
                raise ValueError('"%s" is not a valid SSS variable type' % type_)
        else:
            raise AttributeError("can't set attribute")

    @property
    def ident(self):
        return self._ident

    @ident.setter
    def ident(self, ident):
        # Variable idents must be unique positive integers
        try:
            ident = int(ident)
            assert ident > 0
            assert ident not in self.record.variable_idents
            self._ident = ident
        except (ValueError, AssertionError):
            raise AttributeError(
                'Variable "ident" values must be unique positive '
                'integers ({self.name!r})'.format(self=self))

    @property
    def use(self):
        return self._use

    @use.setter
    def use(self, use):
        if use is None:
            self._use = None
            if hasattr(self.record, '%s_variable' % use):
                if getattr(self.record, '%s_variable' % use) == self:
                    setattr(self.record, '%s_variable' % use, None)

        elif use in self.__usetypes__:
            if self.type in self.__usetypes__[use]:
                if getattr(self.record, '%s_variable' % use) is None:
                    setattr(self.record, '%s_variable' % use, self)
                    self._use = use
                else:
                    raise AttributeError("""can't have more than one "%s" use in a SSS record""" % use)
            else:
                raise ValueError('"%s" is not a valid SSS variable use for variables of type "%s"' % (use, self.type))
        else:
            raise ValueError('"%s" is not a valid SSS variable use' % use)

    @property
    def format(self):
        return self._format

    @format.setter
    def format(self, format):
        if format in self.__varfmts__:
            self._format = format
        else:
            raise ValueError('"%s" is not a valid SSS variable format' % format)

    @property
    def filter_variable(self):
        if self.filter:
            return self.record.variables[self.filter]

    def has_scores(self):
        return self.values.has_scores
    
    def rename(self, new_name, sort_after_rename=True):
        """Rename a variable"""
        if self.name != new_name:
            new_name = self.validate_name(new_name)
            # The new name is valid
            old_name = self.name
            # Rename the variable
            self._name = new_name
            # Delete the old reference in self.record.variables
            del self.record.variables[old_name]
            # Update with this new name
            self.record.variables[new_name] = self
            if sort_after_rename:
                self.record.sort()
    
    def copy_to(self, name, position, ident=None):
        """Create a renamed copy of this variable, and append it to the end of the record"""
        # Validate the new name
        name = self.validate_name(name)

        # Create a shallow copy
        new_self = copy.copy(self)

        # Change the name, ident and position
        new_self._name = name
        if ident is None:
            new_self.ident = self.record.next_variable_ident
        else:
            new_self.ident = ident

        # Next, need to create new instances of Position and Values
        new_self.position = Position(new_self)
        try:
            new_self.position.start = position.start
        except AttributeError:
            try:
                new_self.position.start = position.get('start')
            except AttributeError:
                raise AttributeError('cannot get start position')
    
        try:
            new_self.position.finish = position.finish
        except AttributeError:
            try:
                new_self.position.finish = position.get('finish')
            except AttributeError:
                raise AttributeError('cannot get finish position')
        
        new_self.values = Values(new_self)
        new_self.values.extend(self.values)
        if self.values.range:
            new_self.values.range = Range(
                new_self, 
                from_=self.values.range.from_, 
                to=self.values.range.to
            )

        # Finally, add this new variable to self.record
        self.record.variables[new_self.name] = new_self
        
        return new_self

    def parse_data(self, rawdata):
        if hasattr(rawdata, 'code'):
            return rawdata.code

        if rawdata is None:
            rawdata = ''

        parsed_data = None
        if not '{}'.format(rawdata).strip() and self.type != 'multiple':
            pass
        else:
            if self.type == 'character':
                parsed_data = rawdata.strip()
            elif self.type == 'logical':
                parsed_data = boolean(rawdata)
            elif self.type == 'date':
                dt = datetime.datetime.strptime(rawdata, '%Y%m%d')
                parsed_data = dt.date()
            elif self.type == 'time':
                dt = datetime.datetime.strptime(rawdata, '%H%M%S')
                parsed_data = dt.time()
            elif self.type == 'quantity':
                parsed_data = decimal.Decimal(rawdata)
            elif self.type == 'multiple':
                if self.spread:
                    parsed_data = self.spread.parse_data(rawdata)
                else:
                    parsed_data = MultipleTuple(from_bitstring(rawdata), variable=self)
            elif self.type == 'single':
                if self.format == 'literal':
                    parsed_data = rawdata.strip()
                else:
                    try:
                        parsed_data = int(rawdata)
                    except ValueError:
                        parsed_data = None
            else:
                parsed_data = rawdata
        return parsed_data

    def __str__(self):
        return self.label.__str__()
    
    def __unicode__(self):
        return u'{}'.format(self.label)

    def __html__(self):
        html = '<div class="sss-variable" data-name={self.name!r}>'.format(self=self)
        html += '<div class="sss-name">{self.name}</div>'.format(self=self)
        html += '<div class="sss-label">{self.label}</div>'.format(self=self)
        html += '{}'.format(self.values.__html__())
        html += '</div>'
        return html

    def __repr__(self):
        if self.use:
            return '<Variable({self.name!r}, ' \
                   'type={self.type!r}, ' \
                   'use={self.use!r})>'.format(self=self)
        return '<Variable({self.name!r}, ' \
               'type={self.type!r})>'.format(self=self)
    
    def __xml__(self):
        attrib = OrderedDict([('type', u'{}'.format(self.type)), ('ident', u'{}'.format(self.ident))])
        if self.use:
            attrib['use'] = u'{}'.format(self.use)
        if not self.format == 'numeric':
            attrib['format'] = u'{}'.format(self.format)
        elem = self.etree.Element('variable', **attrib)
        name = self.etree.Element('name')
        name.text = u'{}'.format(self.name)
        elem.append(name)
        label = self.etree.Element('label')
        label.text = u'{}'.format(self.label)
        elem.append(label)
        elem.append(self.position.__xml__())
        if self.filter:
            filter = self.etree.Element('filter')
            filter.text = u'{}'.format(self.filter)
            elem.append(filter)
        if self.spread:
            elem.append(self.spread.__xml__())

        if self.type in self.__values_types__:
            elem.append(self.values.__xml__())
        if self.size:
            size = self.etree.Element('size')
            size.text = u'{}'.format(self.size)
            elem.append(size)
        return elem

    def to_python(self):
        obj = {
            'type': self.type,
            'ident': self.ident,
            'name': self.name,
            'label': self.label,
            'position': self.position.to_python(),
        }

        if self.use:
            obj['use'] = self.use

        if not self.format == 'numeric':
            obj['format'] = self.format

        if self.filter:
            obj['filter'] = self.filter

        if self.spread:
            obj['spread'] = self.spread.to_python()

        if self.type in self.__values_types__:
            obj.update(self.values.to_python())

        if self.size:
            obj['size'] = self.size

        return obj


class Position(XMLObject):
    def __init__(self, variable, start=None, finish=None):
        self.variable = variable
        self.start = start
        self.finish = finish

    def length(self):
        if self.start is not None and self.finish is not None:
            return self.finish - (self.start - 1)
        return 0

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, start):
        if start:
            start = int(start)
        self._start = start

    @property
    def finish(self):
        return self._finish

    @finish.setter
    def finish(self, finish):
        if self.variable.record.format == 'csv':
            finish = None
        else:
            if finish:
                finish = int(finish)
            else:
                finish = self.start

        self._finish = finish

    def colspec(self):
        if self.variable.record.format == 'csv':
            return self.start-1
        #return (self.start-1, self.finish-1)
        return (self.start-1, self.finish)

    def __repr__(self):
        if self.variable.record.format == 'csv':
            return '<Position(%s)>' % self.start
        return '<Position(%s, %s)>' % (self.start, self.finish)
    
    def __xml__(self):
        attrib = OrderedDict([('start', u'{}'.format(self.start))])
        if self.variable.record.format == 'fixed':
            attrib['finish'] = u'{}'.format(self.finish)
        return self.etree.Element('position', **attrib)

    def to_python(self):
        obj = { 'start': self.start }
        if self.variable.record.format == 'fixed':
            obj['finish'] = self.finish
        return obj


class Text(object):
    def __init__(self, default, language_texts=None):
        self.default = default
        if language_texts is None:
            language_texts = {}
        self.language_texts = language_texts

    def lang(self, lang):
        return self.language_texts.get(lang)

    def __repr__(self):
        return "<Text('%s')>" % self.default

    def __str__(self):
        return self.default


class Values(list, XMLObject):
    def __init__(self, variable, range=None, has_scores=False):
        self.variable = variable
        self.range = range
        self.has_scores = has_scores

    @property
    def codes(self):
        return [val.code for val in self]

    @property
    def scores(self):
        return [val.score for val in self]

    @property
    def texts(self):
        return [val.text for val in self]

    def get_by_code(self, code):
        try:
            return [
                v for 
                v in self 
                if v.code == code
            ][0]
        except IndexError:
            pass

    def get_by_text(self, text):
        try:
            return [
                v for 
                v in self 
                if v.text == text
            ][0]
        except IndexError:
            pass

    def remove_by_code(self, code):
        try:
            self.remove(self.get_by_code(code=code))
        except ValueError:
            pass

    @property
    def max_code(self):
        return max(self.codes)

    @property
    def max_code_width(self):
        return len('{}'.format(self.max_code))

    def valid_code(self, code):
        """Is the given code a valid code for this list of values?"""
        valid = code in self.codes
        if not valid and self.range:
            valid = code in self.range
        return valid

    @property
    def range(self):
        return self._range

    @range.setter
    def range(self, range):
        self._range = None
        if range is not None:
            if self.variable.format == 'numeric':
                if isinstance(range, Range):
                    self._range = range
                else:
                    self._range = Range(variable=self.variable,
                                        from_=range.get('from'),
                                        to=range.get('to'))
        elif self.variable.type == 'time':
            self._range = Range(variable=self.variable,
                                from_='000000',
                                to='235959')

    def as_dict(self):
        return {
            value.code:value
            for value
            in self
        }

    def __html__(self):
        # TODO: Print self.range.__html__()
        html = '<ol class="sss-values">'
        for value in self:
            html += '<li value="{value}">{text}</li>'.format(value=value.code,
                                                        text=value.__html__())
        html += '</ol>'
        return html

    def __xml__(self):
        elem = self.etree.Element('values')
        if self.range:
            elem.append(self.range.__xml__())
        for value in self:
            elem.append(value.__xml__())
        return elem

    def update(self, values):
        for value in values:
            pass

    def to_python(self):
        obj = {}

        if self:
            obj['values'] = [
                value.to_python()
                for value
                in self
            ]

        if self.range:
            obj['range'] = self.range.to_python()

        return obj


class Value(XMLObject):
    def __init__(self, values, text, code, score=None):
        self.values = values
        self.text = text
        self.code = code
        self.score = score

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, code):
        if self.values.variable.format == 'literal':
            self._code = code
        else:
            if self.values.variable.type in ('single', 'multiple'):
                self._code = int(code)
            else:
                self._code = decimal.Decimal(code)

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, score):
        self._score = None
        if self.values.variable.type == 'single':
            if score is not None:
                self.values.has_scores = True
                self._score = decimal.Decimal(score)

    def __str__(self):
        return self.text.__str__()

    def __repr__(self):
        repr_template = '<Value({self.text!r}, code={self.code}'
        if self.score is not None:
            repr_template += ', score={self.score}'
        repr_template += ')>'
        return repr_template.format(self=self)

    def __html__(self):
        html_template = '<span class="sss-value" data-code="{self.code}"'
        if self.score is not None:
            html_template += ' data-score="{self.score}"'
        html_template += '>{self.text}</span>'
        return html_template.format(self=self)

    def __xml__(self):
        attrib = OrderedDict([('code', u'{}'.format(self.code))])
        if not self.score is None:
            attrib['score'] = u'{}'.format(self.score)
        elem = self.etree.Element('value', **attrib)
        elem.text = u'{}'.format(self.text)
        return elem

    def to_python(self):
        obj = {
            'text': self.text,
            'code': self.code,
        }
        if self.score is not None:
            obj['score'] = self.score
        return obj


class Range(XMLObject):
    def __init__(self, variable, from_, to):
        self.variable = variable
        self.from_ = from_
        self.to = to

    def _parse_range_part(self, item):
        """Parse the item and convert it into a suitable type for the 
        associated variable.
        """
        if self.variable.type == 'date':
            if not isinstance(item, datetime.date):
                item = datetime.datetime.strptime('{}'.format(item), '%Y%m%d')
            return datetime.date(year=item.year, month=item.month, day=item.day)
        elif self.variable.type == 'time':
            if isinstance(item, six.string_types):
                hour, minute, second = int(item[:2]), int(item[2:4]), int(item[4:6])
                return datetime.time(hour, minute, second)
            return datetime.time(item.hour, item.minute, item.second)
        return decimal.Decimal(item)

    @property
    def from_(self):
        return self._from

    @from_.setter
    def from_(self, from_):
        self._from = self._parse_range_part(from_)

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, to):
        _to = self._parse_range_part(to)
        if _to >= self.from_:
            self._to = _to
        else:
            raise AttributeError('The "to" attribute must be greater than or equal to the "from" attribute: %s <= %s' % (self.from_, _to))

    @property
    def From(self):
        return self.from_

    def __xml__(self):
        attrib = OrderedDict([
            ('from', u'{}'.format(self.from_).replace(':', '')),
            ('to', u'{}'.format(self.to).replace(':', '')),
        ])
        elem = self.etree.Element('range', **attrib)
        return elem

    def to_python(self):
        return {
            'from': self.from_,
            'to': self.to,
        }

    def is_float(self):
        return bool(
            '.' in '{}'.format(self.From) or
            '.' in '{}'.format(self.to)
        )

    def __contains__(self, value):
        try:
            item = self._parse_range_part(value)
            return item >= self.from_ and item <= self.to
        except ValueError:
            return False

    def __repr__(self):
        return '<Range(%s, %s)>' % (self.from_, self.to)


class Spread(XMLObject):
    def __init__(self, variable, subfields=None, width=None):
        self.variable = variable
        self.subfields = subfields
        self.width = width

    @property
    def subfields(self):
        return self._subfields

    @subfields.setter
    def subfields(self, subfields):
        try:
            self._subfields = int(subfields)
        except TypeError:
            self._subfields = None

    @property
    def width(self):
        if self._width is None and self.subfields is not None:
            if self.variable.record.format == 'fixed':
                return self.subfields / self.variable.position.length()
        return self._width

    @width.setter
    def width(self, width):
        try:
            self._width = int(width)
        except TypeError:
            self._width = None
    
    def parse_data(self, rawdata):
        return MultipleTuple(
            from_spread(
                number_string=rawdata,
                width=self.width,
                subfields=self.subfields
            ),
            variable=self.variable
        )

    def join_data(self, data, width=None):
        if width is None:
            width = self.width
        return to_spread(
            numbers=data,
            width=width
        )

    def __repr__(self):
        return u'<Spread(subfields={self.subfields}, width={self.width})>'.format(self=self)
        
    def __xml__(self):
        attrib = OrderedDict()
        if self.subfields:
            attrib['subfields'] = u'{}'.format(self.subfields)
        if self.width:
            attrib['width'] = u'{}'.format(self.width)
        elem = self.etree.Element('spread', **attrib)
        return elem

    def to_python(self):
        obj = {}
        if self.subfields:
            obj['subfields'] = self.subfields
        if self.width:
            obj['width'] = self.width
        return obj