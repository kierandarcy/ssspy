from warnings import warn

import pandas as pd
from pandas.api.types import CategoricalDtype
from pandas.core.tools.datetimes import to_time

from . import meta
from . import data


def index_col(sss):
    return sss.variables.serial_name


def dtypes(sss):
    dtype = {}

    # Logical as bool
    dtype.update({
        name: 'bool'
        for name
        in sss.variables.by_type('logical')
    })

    # Multiples as objects
    dtype.update({
        name: 'object'
        for name
        in sss.variables.by_type('multiple')
    })

    # Literal singles as objects
    dtype.update({
        name: 'str'
        for name
        in sss.variables.by_type('single').literals()
    })

    # Non-literal singles as ints
    dtype.update({
        name: 'int64'
        for name
        in sss.variables.by_type('single').literals(drop=True)
    })

    # Quantity as int or float
    dtype.update({
        name: 'float64'
        for name
        in sss.variables.floats()
    })

    return dtype


def get_categories(variables):
    return {
        variable.name: CategoricalDtype([
            '{}'.format(c)
            for c
            in variable.values.codes
        ], ordered=False)
        for variable
        in variables.values()
    }


def set_categories(sss, df, inplace=False, variable_names=None):
    variables = sss.variables.by_type('single')

    if variable_names:
        variables = variables.by_name(*variable_names)

    if not inplace:
        df = df.copy()

    # Singles as category
    for name, category in get_categories(variables).items():
        df[name] = df[name].astype('str').astype(dtype=category)

    if not inplace:
        return df


def converters(sss):
    _converters = {}

    # Time as time
    _converters.update({
        name: to_time
        for name
        in sss.variables.by_type('time')
    })

    return _converters


def read_arguments(sss):
    kwargs = {
        'dtype': dtypes(sss),
        'parse_dates': list(sss.variables.by_type('date')),
        'converters': converters(sss),
        'index_col': index_col(sss),
    }

    if sss.survey.record.format == 'fixed':
        if sss.survey.record.skip:
            kwargs['skiprows'] = [sss.survey.record.skip - 1]
        kwargs['header'] = None
        kwargs['colspecs'] = tuple(sss.survey.record.colspecs.values())
        kwargs['names'] = tuple(sss.survey.record.colspecs)

    return kwargs


def read_sss(sss, datafile=None, extend=False, **kwargs):
    if not isinstance(sss, meta.SSS):
        sss = meta.parse(sss)

    kwargs.update(read_arguments(sss))

    if sss.survey.record.format == 'csv':
        df = pd.read_csv(datafile, **kwargs)
    else:
        df = pd.read_fwf(datafile, **kwargs)

    # Singles need to have their categories set
    set_categories(sss=sss, df=df, inplace=True)

    index_column = kwargs.get('index_col')

    if index_column:
        df = df.reset_index().set_index(index_column, drop=False)

    if extend:
        extend_dataframe(sss=sss, df=df, inplace=True, 
                         add_missing_columns=False)

    return df


class DataParser:
    __slots__ = (
        'variable',
        'name',
        '_parser'
    )

    def __init__(self, variable):
        self.variable = variable
        self.name = 'parse_{}'.format(variable.name)

    @property
    def parser(self):
        if not hasattr(self, '_parser'):
            def func(x):
                return data.DataField(self.variable, x)
            self._parser = func
        return self._parser

    def __repr__(self):
        return '{self.name}'.format(self=self)

    def __call__(self, x):
        return self.parser(x)


def data_parsers(sss):
    # Add text, code and score fields
    # Add texts
    return {
        name: DataParser(variable)
        for name, variable
        in sss.variables.by_type('single', 'multiple').items()
    }


def multiple_variable_columns(sss):
    return {
        name: [
            '{}#{}'.format(name, code)
            for code
            in variable.values.codes
        ]
        for name, variable
        in sss.variables.by_type('multiple').items()
    }


def get_missing_columns(sss, df):
    return [
        name
        for name 
        in sss.variables
        if name not in df.columns
    ]


def concat_missing_columns(sss, df):
    columns = get_missing_columns(sss=sss, df=df)

    return pd.concat([
        df,
        pd.DataFrame(columns=columns)
    ])


def extend_dataframe(sss, df, inplace=False, variable_names=None,
                     add_missing_columns=True):
    if not inplace:
        df = df.copy()

    parsers = data_parsers(sss)

    variables = sss.variables.by_type('multiple', 'single')

    if variable_names:
        variables = variables.by_name(*variable_names)

    for variable in variables.values():
        name = variable.name
        parser = parsers[name]

        if add_missing_columns and name not in df.columns:
            df[name] = pd.np.NaN

        if variable.has_scores():
            def score_parser(x):
                return parser(x).score

            df['{}.score'.format(name)] = df[name] \
                .dropna() \
                .astype('object') \
                .map(score_parser) \
                .astype('float64')

        def text_parser(x):
            return parser(x).label(pretty=True)

        df['{}.text'.format(name)] = df[name]\
            .dropna()\
            .astype('object')\
            .map(text_parser)

        if variable.type == 'single':

            df['{}.text'.format(name)] = df['{}.text'.format(name)]\
                .astype(
                    CategoricalDtype(
                        categories=list(variable.values.texts),
                        ordered=False
                    )
                )

        elif variable.type == 'multiple':

            def code_parser(x):
                return parser(x).code

            df['{}.code'.format(name)] = df[name] \
                .dropna() \
                .astype('object') \
                .map(code_parser)

            new_codes = list(variable.values.codes)
            new_texts = list(variable.values.texts)
            new_columns = [
                '{}#{}'.format(name, code)
                for code
                in new_codes
            ]
            new_text_columns = [
                '{}.text#{}'.format(name, code)
                for code
                in new_codes
            ]

            for idx, new_column in enumerate(new_columns):
                df[new_column] = pd.np.NaN
                df[new_column] = df[new_column].astype(
                    CategoricalDtype(
                        categories=['{}'.format(new_codes[idx])]
                    )
                )

            for idx, new_text_column in enumerate(new_text_columns):
                df[new_text_column] = pd.np.NaN
                df[new_text_column] = df[new_text_column].astype(
                    CategoricalDtype(
                        categories=[new_texts[idx]]
                    )
                )

            def exploder(vname):
                def func(row):
                    if pd.notnull(row['{}.code'.format(vname)]):
                        for code in row['{}.code'.format(vname)]:
                            try:
                                row[new_columns[new_codes.index(code)]] = \
                                    '{}'.format(code)
                                row[new_text_columns[new_codes.index(code)]] = \
                                    new_texts[new_codes.index(code)]
                            except ValueError:
                                pass
                    return row[new_columns + new_text_columns]
                return func

            variable_df = df.apply(
                exploder(name),
                axis=1
            )

            for new_column in new_columns:
                df[new_column] = variable_df[new_column]

            for new_text_column in new_text_columns:
                df[new_text_column] = variable_df[new_text_column]

    if not inplace:
        return df
